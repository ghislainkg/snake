# Snake

![alt text](snake.png "Snake")

# Build and Run

Make sure to have GTK+3 installed:
```bash
sudo apt-get install build-essential libgtk-3-dev
```
Then in the project directory:
```bash
mkdir build
cd build

cmake ..
make
```

Run:
```bash
./snake
```
