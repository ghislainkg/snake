#include "player_control.h"

static void player_control_on_direction_change(Direction direction, Map * map, int playerSnakeId) {
    switch (direction) {
    case LEFT:
        snake_change_direction(map->snakeList->liste[playerSnakeId], LEFT);
        break;
    case RIGHT:
        snake_change_direction(map->snakeList->liste[playerSnakeId], RIGHT);
        break;
    case UP:
        snake_change_direction(map->snakeList->liste[playerSnakeId], UP);
        break;
    case DOWN:
        snake_change_direction(map->snakeList->liste[playerSnakeId], DOWN);
        break;
    }
}

static void player_control_toggle_pause(Map * map) {
    map->isPaused = (map->isPaused)? FALSE : TRUE;
}

/*La fonction pour gerer les boutons des joueurs en local*/
gboolean player_control_on_key_release(GtkWidget *widget, GdkEvent *event, Map * map) {
	GdkEventKey * ev = (GdkEventKey *) event;

	switch(ev->keyval) {
		case GDK_KEY_Left: // direction
			player_control_on_direction_change(LEFT, map, 0);
			break;
		case GDK_KEY_Up: // direction
			player_control_on_direction_change(UP, map, 0);
			break;
		case GDK_KEY_Right: // direction
			player_control_on_direction_change(RIGHT, map, 0);
			break;
		case GDK_KEY_Down: // direction
			player_control_on_direction_change(DOWN, map, 0);
			break;

		case GDK_KEY_space: // pause
			player_control_toggle_pause(map);

		case GDK_KEY_a: // direction
			player_control_on_direction_change(LEFT, map, 1);
			break;
		case GDK_KEY_w: // direction
			player_control_on_direction_change(UP, map, 1);
			break;
		case GDK_KEY_d: // direction
			player_control_on_direction_change(RIGHT, map, 1);
			break;
		case GDK_KEY_s: // direction
			player_control_on_direction_change(DOWN, map, 1);
			break;

	}
	return TRUE;
}
