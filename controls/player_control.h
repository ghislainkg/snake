#ifndef _PLAYER_CONTROL_
#define _PLAYER_CONTROL_

#include "../entities/map.h"

#include "../help_structs/help_ui.h"

/*TODO
- Lire les touches de base des joueurs dans un fichier de configuration
- Limiter le nombre de joueurs sur le clavier a 4
*/

gboolean player_control_on_key_release(GtkWidget *widget, GdkEvent *event, Map * map);

#endif