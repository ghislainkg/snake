#include "init/game_window.h"
#include "init/settings_window.h"

int main(int argc, char *argv[]) {

	srand(time(NULL));

	GtkApplication * app;
	app = gtk_application_new("gumeteapps.snake", G_APPLICATION_FLAGS_NONE);
	g_signal_connect_swapped(app, "activate",
		G_CALLBACK(create_game_settings_window), create_game_window);

	int status = g_application_run(G_APPLICATION(app), argc, argv);

	g_object_unref(app);

	return status;
}