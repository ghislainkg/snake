#ifndef _SETTINGS_WINDOW_
#define _SETTINGS_WINDOW_

#include "../help_structs/help_ui.h"

typedef void (*FuncSettingsWindowLaunchGame) (GameSettings settings);

void create_game_settings_window(FuncSettingsWindowLaunchGame launchGame);

#endif