#include "settings_window.h"

static void onStartGame(void ** data) {
    GameSettings settings;
    GtkWidget * playerCountEntry = data[0];
    GtkWidget * snakeCountEntry = data[1];
    GtkWidget * blockCountEntry = data[2];
    FuncSettingsWindowLaunchGame launchGameFunction = data[3];

    int playerCount;
    int snakeCount;
    int blockCount;
    const char * pText = gtk_entry_get_text(GTK_ENTRY(playerCountEntry));
    sscanf(pText, "%d", &playerCount);
    const char * sText = gtk_entry_get_text(GTK_ENTRY(snakeCountEntry));
    sscanf(sText, "%d", &snakeCount);
    const char * bText = gtk_entry_get_text(GTK_ENTRY(blockCountEntry));
    sscanf(bText, "%d", &blockCount);

    printf("player=%d snake=%d block=%d\n", playerCount, snakeCount, blockCount);

    settings.playerCount = playerCount;
    settings.snakeCount = snakeCount;
    settings.blockCount = blockCount;

    launchGameFunction(settings);
}

void create_game_settings_window(FuncSettingsWindowLaunchGame launchGameFunction) {
	GtkWidget * window;
    GtkWidget * playerCountEntry;
    GtkWidget * snakeCountEntry;
    GtkWidget * blockCountEntry;
    GtkWidget * startGameButton;
    GtkWidget * cancelGameButton;

	/*On recupere le modele de fenetre glade*/
	GtkBuilder * builder = gtk_builder_new_from_file(UI_FILE);
	EXITS_ON_NULL(builder)

	window = GTK_WIDGET(gtk_builder_get_object(builder, "setting_window"));
	EXITS_ON_NULL(window)
	playerCountEntry = GTK_WIDGET(gtk_builder_get_object(builder, "player_count_entry"));
	EXITS_ON_NULL(playerCountEntry)
    snakeCountEntry = GTK_WIDGET(gtk_builder_get_object(builder, "snake_count_entry"));
	EXITS_ON_NULL(snakeCountEntry)
    blockCountEntry = GTK_WIDGET(gtk_builder_get_object(builder, "block_count_entry"));
	EXITS_ON_NULL(blockCountEntry)
    startGameButton = GTK_WIDGET(gtk_builder_get_object(builder, "start_game_button"));
	EXITS_ON_NULL(startGameButton)
    cancelGameButton = GTK_WIDGET(gtk_builder_get_object(builder, "cancel_game_button"));
	EXITS_ON_NULL(cancelGameButton)

    void * args[] = {playerCountEntry, snakeCountEntry, blockCountEntry, launchGameFunction};
    g_signal_connect_swapped(startGameButton, "clicked",
        G_CALLBACK(onStartGame), args);

    g_signal_connect_swapped(cancelGameButton, "clicked",
        G_CALLBACK(gtk_main_quit), NULL);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    gtk_widget_show_all(window);

	gtk_main();
}
