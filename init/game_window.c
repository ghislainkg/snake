#include "game_window.h"

/*Fonction du executee par le thread de la map (modele) de jeu*/
static gboolean map_thread_execution(Map * map) {
	play_round(map);

	return G_SOURCE_CONTINUE;
}
static gpointer map_thread(Map * map) {	
	g_timeout_add(100, (GSourceFunc) map_thread_execution, map);
	return NULL;
}

/**Initialise la map en fonction de width et height
 * Et lance le thread de la map.*/
static void init_start_game(Map * map, GameSettings * settings, int width, int height) {
	printf("Game field width=%d height=%d\n", width, height);

	// On fixe les limite de la map
	map_set_constraint(
		map,
		settings->playerCount,
		width/10, 0, 0, height/10
	);

	SnakeList * snakeList;
	BlockList * blockList;
	entitiesgen_gen_random_snakes_blocks_for_map(
        map,
        &snakeList, settings->snakeCount, 2,
        &blockList, settings->blockCount);
	map_set_snakelist(map, snakeList);
	map_set_blocklist(map, blockList);

	map->isReady = TRUE;
	map->isPaused = FALSE;
	/*GThread * thread = */g_thread_new("snake_map_thread", (GThreadFunc) map_thread, map);
}

/*La fonction appellee a chaque dessin*/
static void on_draw(GtkWidget * drawarea, cairo_t * context, void ** data) {
    GameSettings * settings = data[0];
    Map * map = data[1];

	int width = gtk_widget_get_allocated_width(drawarea);
	int height = gtk_widget_get_allocated_height(drawarea);

	if(map->isReady) {
		// On dessine la map
		draw_map(context, map);

		// On demande a gtk de redessiner la totalite de motre drawarea
		gtk_widget_queue_draw_area(drawarea, 0, 0, width, height);
	}
	else {
		// On initialise la map
		init_start_game(map, settings, width, height);
	}
}

/*Fonction de creation de la fenetre GTK et du modele du jeu*/
void create_game_window(GameSettings settings) {

	// LA FENETRE
	GtkWidget * window;
	GtkWidget * drawarea;

	/*On recupere le modele de fenetre glade*/
	GtkBuilder * builder = gtk_builder_new_from_file(UI_FILE);
	EXITS_ON_NULL(builder)

	window = GTK_WIDGET(gtk_builder_get_object(builder, "game_window"));
	EXITS_ON_NULL(window)
	drawarea = GTK_WIDGET(gtk_builder_get_object(builder, "drawarea"));
	EXITS_ON_NULL(drawarea)

	// MODELE DU JEU

	// On cree la map
	Map * map = map_create();

	// CONNEXION DE SIGNAUX

	// Signal de dessin
    void * args[] = {&settings, map};
	g_signal_connect(drawarea, "draw", G_CALLBACK(on_draw), args);

	/*On active les evenements de la fenetre*/
	gtk_widget_add_events(window, GDK_KEY_RELEASE_MASK);
	g_signal_connect(window, "key-release-event", 
        G_CALLBACK(player_control_on_key_release), map);
	g_signal_connect(window, "destroy",
        G_CALLBACK(gtk_main_quit), NULL);

	gtk_widget_show_all(window);

	gtk_main();
}