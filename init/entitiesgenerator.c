#include "entitiesgenerator.h"

static void getPosInLimit(
    Map * map,
    int * x, int * y) {

    *x = rand() % map->limitR + map->limitL;
    *y = rand() % map->limitD + map->limitU;
}

static int isInPosTab(
    Point * tab, unsigned tabSize,
    int x, int y) {

    for(unsigned i = 0; i < tabSize; i++) {
        int x0 = tab[i].x;
        int y0 = tab[i].y;
        if(x0 == x && y0 == y) {
            return 1;
        }
    }
    return 0;
}

static void getPosInLimitNotInPosTab(
    Point * tab, unsigned tabSize,
    Map * map,
    int * x, int * y) {

    do{
        getPosInLimit(map, x, y);
        if(tab == NULL || tabSize <= 0) return;
    }
    while(isInPosTab(tab, tabSize, *x, *y));
}

BlockList * entitiesgen_gen_random_blocks(
    Map * map,
    int nbrBlocks,
    Point * takenPos, unsigned takenPosSize) {

    BlockList * blockList = blocklist_create(0);
    int x, y;
	for (int i = 0; i < nbrBlocks; ++i) {
        getPosInLimitNotInPosTab(takenPos, takenPosSize, map, &x, &y);
        BlockType type;
        Block * block;
        switch(rand() % 2) {
        case 0:
            type = HELL;
            block = block_create_square(x, y, 2, type);
            break;
        case 1:
            type = LIFE;
            block = block_create_square(x, y, 0, type);
            break;
        }
		blocklist_add(blockList, block);
	}

    return blockList;
}

static Color entitiesgen_get_color_for_snake_id(int id, unsigned playersCount) {
    if(id < playersCount) {
        // GREEN
        Color color = {0.1f, 1.0f, 0.1f};
        return color;
    }
    else {
        // RED
        Color color = {1.0f, 0.5f, 0.5f};
        return color;
    }
}

SnakeList * entitiesgen_gen_random_snakes(
    Map * map,
    int snakesSize,
    int nbrSnakes,
    Point * takenPos, unsigned takenPosSize) {

	SnakeList * snakeList = snakelist_create(0);
    int x, y;
	for (unsigned id = 0; id < nbrSnakes; ++id) {
        getPosInLimitNotInPosTab(takenPos, takenPosSize, map, &x, &y);

        Color color = entitiesgen_get_color_for_snake_id(id, map->playerCount);

        Snake * snake;
		if(id < map->playerCount) {
            snake = snake_create(snakesSize, color, FALSE, id, x, y);
        }
        else {
            snake = snake_create(snakesSize, color, TRUE, id, x, y);
        }
		snakelist_add(snakeList, snake);
	}

	return snakeList;
}

static void fillTakenPos(int i, Block * b, BlockList * l, void ** args) {
    Point * takenPos = args[0];
    int * takenPosI = args[1];
    for(unsigned j=0; j < b->size; j++, (*takenPosI)++) {
        (takenPos)[(*takenPosI)] = b->points[j];
    }
}
static void countBlockPosition(Block * block, BlockList * blockList, int * posCount) {
    (*posCount) += block->size;
}
void entitiesgen_gen_random_snakes_blocks_for_map(
    Map * map,
    SnakeList ** snakeList, unsigned int snakeCount, unsigned int snakeSize,
    BlockList ** blockList, unsigned int blockCount) {
    
    /* Generons les blocks*/
    *blockList = entitiesgen_gen_random_blocks(map, blockCount, NULL, 0);

    /* Remplissons un table contenant les points
    pris par les blocks*/
    unsigned takenPosCount;
    blocklist_foreach(
        *blockList,
        (Func_Blocklist_foreach_callback) countBlockPosition,
        &takenPosCount);
    Point takenPos[takenPosCount];
    unsigned takenPosI = 0;
    void * args[] = {&takenPos, &takenPosI};
    blocklist_foreach_index(*blockList, (Func_Blocklist_foreach_index_callback) fillTakenPos, args);

    /*Generons les snakes avec leur tetes a des positions non 
    occupees par des blocks*/
    *snakeList = entitiesgen_gen_random_snakes(
        map, snakeSize, snakeCount,
        takenPos, takenPosCount);
}
