#ifndef _GAME_WINDOW_
#define _GAME_WINDOW_

#include "../help_structs/help_ui.h"

#include "../draw/drawing.h"

#include "entitiesgenerator.h"
#include "../entities/map.h"
#include "../controls/player_control.h"

void create_game_window(GameSettings settings);

#endif
