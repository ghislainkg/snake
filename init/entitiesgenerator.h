#ifndef _ENTITIES_GENERATOR_
#define _ENTITIES_GENERATOR_

#include <time.h>

#include <stdio.h>

#include "../entities/map.h"

/*Retourne une liste de blocks aleatoire*/
BlockList * entitiesgen_gen_random_blocks(
    Map * map,
    int nbrBlocks,
    Point * takenPos, unsigned takenPosSize);

/*Retourne une liste de snakes aleatoires*/
SnakeList * entitiesgen_gen_random_snakes(
    Map * map,
    int snakesSize,
    int nbrSnakes,
    Point * takenPos, unsigned takenPosSize);

void entitiesgen_gen_random_snakes_blocks_for_map(
    Map * map,
    SnakeList ** snakeList, unsigned int snakeCount, unsigned int snakeSize,
    BlockList ** blockList, unsigned int blockCount);

#endif