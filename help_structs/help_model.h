#ifndef _HELP_MODEL_
#define _HELP_MODEL_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

typedef struct Color {
    float r;
    float g;
    float b;
} Color;

enum Direction {
	RIGHT,
	LEFT,
	UP,
	DOWN
};
typedef enum Direction Direction;

typedef struct Point {
	int x;
	int y;
} Point;

#endif