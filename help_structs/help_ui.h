#ifndef _HELP_UI_
#define _HELP_UI_

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

#include <gtk/gtk.h>

#define UI_FILE "ui.glade"

#define EXITS_ON_NULL(pointer) \
	if(pointer == NULL) { \
		exit(1); \
	}

typedef struct GameSettings{
    unsigned playerCount;
    unsigned snakeCount;
    unsigned blockCount;
} GameSettings;

#endif