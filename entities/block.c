#include "block.h"

Block * block_create_single(int x, int y, BlockType type) {
	Block b;
	b.points = malloc(sizeof(Point));
	b.points[0].x = x;
	b.points[0].y = y;
	b.size = 1;
	b.type = type;

	Block * block = malloc(sizeof(Block));
	*block = b;

	return block;
}

Block * block_create_square(int x, int y, unsigned thickness, BlockType type) {
	Block block;

	unsigned base = 2*thickness+1;
	unsigned positionCount = base*base;
	block.points = malloc(positionCount*sizeof(Point));
	block.size = positionCount;
	for(unsigned i=0; i<positionCount; i++) {
		int a = (i/base) - thickness;
		int b = (i%base) - thickness;
		block.points[i].x = x + a;
		block.points[i].y = y + b;
	}

	block.type = type;

	Block * bl = malloc(sizeof(Block));
	*bl = block;
	return bl;
}

void block_destroy(Block * b) {
	free(b);
}

Color block_get_color_for_type(BlockType type) {
	switch(type) {
	case HELL:
		// RED
		return (Color) {1.0f, 0.1f, 0.1f};
	case LIFE:
		// GREEN
		return (Color) {0.1f, 1.0f, 0.1f};
	default:
		// BLACK
		return (Color) {1.0f, 1.0f, 1.0f};
	}
}

short block_collision_with(Block * block, Point point) {
	for(unsigned i=0; i<block->size; i++) {
		if(block->points[i].x == point.x &&
			block->points[i].y == point.y) {
				return TRUE;
		}
	}
	return FALSE;
}


BlockList * blocklist_create(int count) {
	BlockList * b = malloc(sizeof(BlockList));
	b->liste = malloc(sizeof(Block*)*BLOCK_LIST_SIZE_LIMIT);
	b->size = count;
	b->total = BLOCK_LIST_SIZE_LIMIT;

	return b;
}
void blocklist_add(BlockList *l, Block *b) {
	l->liste[l->size] = b;
	l->size++;
}
void blocklist_remove_free(BlockList *l, Block *b) {
	for (unsigned i = 0; i < l->size; ++i) {
		if(l->liste[i] == b) {
			l->liste[i] = NULL;
			l->size--;
			return;
		}
	}
}
void blocklist_destroy(BlockList * b) {
	free(b->liste);
	free(b);
}

void blocklist_foreach(BlockList * l, Func_Blocklist_foreach_callback callback, void * data) {
	for (unsigned i = 0; i < l->size; ++i) {
		Block * current = l->liste[i];
		if(current != NULL) {
			callback(current, l, data);
		}
	}
}

void blocklist_foreach_index(BlockList * l, Func_Blocklist_foreach_index_callback callback, void * data) {
	for (unsigned i = 0; i < l->size; ++i) {
		Block * current = l->liste[i];
		if(current != NULL) {
			callback(i, current, l, data);
		}
	}
}
