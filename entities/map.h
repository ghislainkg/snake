#ifndef _MAP_
#define _MAP_

#include "../help_structs/help_model.h"

#include "block.h"
#include "snake.h"

/*TODO
- Gerer les collisions entre snakes

- map => utf8 (Vas servir pour la sauvegarde et pour envoyer la map sur le reseau)
- utf8 => map (Vas servir pour la sauvergarde et pour lire une map venant du reseau)
*/

/* Definition d'une Map*/
typedef struct Map {

	// Est TRUE si la map est prette a etre utilise, FALSE sinon
	short isReady;

	// Est TRUE si la map est en pause, FALSE sinon
	short isPaused;

	// La liste des blocks
	BlockList * blockList;
	// La liste des snakes
	SnakeList * snakeList;
	// Le nombre de joueur
	unsigned playerCount; 

	// Les limites de la map
	int limitR; int limitL; int limitU; int limitD;
} Map;

/*Cree une map vide*/
Map * map_create();
/*Detruit une map*/
void map_destroy(Map * m);
/*Fixe la liste de snakes d'une map*/
void map_set_snakelist(Map * m, SnakeList * l);
/*Fixe la liste de blocks d'une map*/
void map_set_blocklist(Map * m, BlockList * l);
/*Fixe les limites d'une map*/
void map_set_constraint(
	Map * map,
	unsigned playerCount,
	int limitR, int limitL, int limitU, int limitD);

/*Jou un tour de jeu*/
void play_round(Map * m);

#endif