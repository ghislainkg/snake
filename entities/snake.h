#ifndef _SNAKE_
#define _SNAKE_

#include "../help_structs/help_model.h"

/*Definitions des snakes*/
typedef struct Snake {
	/*Un tableau de <size> points*/
	Point * points;
	unsigned size;
	Color color;

	/*Une direction courante*/
	Direction direction;

	/*Un id*/
	unsigned id;

	/*Un score*/
	unsigned score;

	/*Vivant ou mort*/
	short isAlive;

	/*is computer*/
	short isComputer;
} Snake;

/*Taille limite d'un snake (nombre de point maximum)*/
#define SNAKE_SIZE_LIMIT 20

/*Fonction pour creer un snake*/
Snake * snake_create(
	unsigned size, // La taille du snake
	Color color, // La couleur
	short isComputer,
	unsigned id, // L'id 
	int x, int y // La position dans le plan
);
/*Ajoute un point a un snake*/
void snake_increment_size(Snake * snake);
void snake_increment_score(Snake * snake);
/*Reduit un point a un snake*/
void snake_decrement_size(Snake * snake);
void snake_decrement_score(Snake * snake);
/*Change la direction courante d'un snake*/
void snake_change_direction(Snake * snake, Direction Direction);
/*Change la position d'un snake suivant sa direction*/
void snake_move(Snake * snake);
/*Detruit un snake*/
void snake_destroy(Snake * snake);


/*STRUCTURE DE LISTE DE SNAKE*/

typedef struct SnakeList {
	Snake ** liste;
	unsigned int size;
	unsigned int total;
} SnakeList;

#define SNAKE_LIST_SIZE_LIMIT 100

typedef void (*Func_Snakelist_foreach_callback)(Snake *s, SnakeList * snakeList, void * data);

SnakeList * snakelist_create(int count);
void snakelist_add(SnakeList *l, Snake *b);
void snakelist_remove_free(SnakeList *l, Snake *b);
void snakelist_destroy(SnakeList * b);
void snakelist_foreach(SnakeList * l, Func_Snakelist_foreach_callback callback, void * data);

#endif