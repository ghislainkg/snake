#include "snake.h"

Snake * snake_create(unsigned size, Color color, short isComputer, unsigned id, int x, int y) {
	Snake s;
	s.size = size;
	s.points = malloc(sizeof(Point) * SNAKE_SIZE_LIMIT);
	s.direction = RIGHT;
	s.id = id;
	s.color = color;
	s.score = 0;
	s.isAlive = TRUE;
	s.isComputer = isComputer;
	
	Snake * snake = malloc(sizeof(Snake));
	*snake = s;

	for (unsigned i = 0; i < snake->size; ++i) {
		if(i == 0) {
			snake->points[i].x = x;
			snake->points[i].y = y;
		}
		else {
			snake->points[i].x = x - i;
			snake->points[i].y = y;
		}
	}
	return snake;
}

void snake_increment_size(Snake * snake) {
	snake->points[snake->size].x = snake->points[snake->size-1].x;
	snake->points[snake->size].y = snake->points[snake->size-1].y;
	snake_move(snake);
	snake->size++;
}

void snake_increment_score(Snake * snake) {
	snake->score++;
}

void snake_decrement_size(Snake * snake) {
	snake->size--;
}

void snake_decrement_score(Snake * snake) {
	snake->score--;
}

void snake_change_direction(Snake * snake, Direction direction) {
	snake->direction = direction;
}

void snake_move(Snake * snake) {
	for (unsigned i=snake->size-1; i>=1; i--) {
		//Echange des position du point precedent et du suivant
		Point * point = &(snake->points[i]);
		Point * prev = &(snake->points[i-1]);
		point->x = prev->x;
		point->y = prev->y;
	}
	// Changeons la position de la tete
	switch(snake->direction) {
		case RIGHT:
			snake->points[0].x++;
			break;
		case LEFT:
			snake->points[0].x--;
			break;
		case UP:
			snake->points[0].y--;
			break;
		case DOWN:
			snake->points[0].y++;
			break;
	}
}

void snake_destroy(Snake * snake) {
	free(snake->points);
	free(snake);
}


SnakeList * snakelist_create(int count) {
	SnakeList * b = malloc(sizeof(SnakeList));
	b->liste = malloc(sizeof(Snake*)*SNAKE_LIST_SIZE_LIMIT);
	b->size = count;
	b->total = SNAKE_LIST_SIZE_LIMIT;

	return b;
}
void snakelist_add(SnakeList *l, Snake *b) {
	l->liste[l->size] = b;
	l->size++;
}
void snakelist_remove_free(SnakeList *l, Snake *b) {
	for (unsigned i = 0; i < l->size; ++i) {
		if(l->liste[i] == b) {
			snake_destroy(l->liste[i]);
			l->liste[i] = NULL;
			l->size--;
			return;
		}
	}
}
void snakelist_destroy(SnakeList * b) {
	free(b->liste);
	free(b);
}

void snakelist_foreach(SnakeList * l, Func_Snakelist_foreach_callback callback, void * data) {
	for (unsigned i = 0; i < l->size; ++i) {
		Snake * current = l->liste[i];
		if(current != NULL) {
			callback(current, l, data);
		}
	}
}

