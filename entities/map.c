#include "map.h"

Map * map_create() {
	Map * m = malloc(sizeof(Map));
	memset(m, 0, sizeof(Map));
	m->isReady = FALSE;
	m->isPaused = TRUE;
	return m;
}

void map_destroy(Map * m) {
	free(m);
}

void map_set_snakelist(Map * m, SnakeList * l) {
	m->snakeList = l;
}

void map_set_blocklist(Map * m, BlockList * l) {
	m->blockList = l;
}

void map_set_constraint(
	Map * map,
	unsigned playerCount,
	int limitR, int limitL, int limitU, int limitD) {

	map->limitR = limitR;
	map->limitL = limitL;
	map->limitU = limitU;
	map->limitD = limitD;
	map->playerCount = playerCount;
}







/*Teste si un snake est hors de la map
et le renvoie sa tete dans le cote opposee*/
static int offLimit(Map * map, Snake * snake) {
	int * x = &(snake->points[0].x);
	int * y = &(snake->points[0].y);

	if(*x > map->limitR) {
		*x = map->limitL;
		return 1;
	}
	if(*x < map->limitL) {
		*x = map->limitR;
		return 1;
	}
	if(*y > map->limitD) {
		*y = map->limitU;
		return 1;
	}
	if(*y < map->limitU) {
		*y = map->limitD;
		return 1;
	}

	return 0;
}
/*Deplace un snake*/
static void move_snake(Snake * snake, SnakeList * snakeList, Map * map) {
	if(!snake->isAlive) return;
	offLimit(map, snake);
	snake_move(snake);
}

/*Teste si le block et le snake specifies sont en collision*/
static void check_snake_vs_block(Block * block, BlockList * blockList, Snake * snake) {
	if(block_collision_with(block, snake->points[0])) {
		/*S'il son en collision*/

		switch(block->type) {
		case LIFE:
			// Le snake mange le block
			snake_increment_size(snake);
			snake_increment_score(snake);
			printf("Snake %d : %d", snake->id, snake->score);
			break;
		case HELL:
			// Le snake pert un block
			snake_decrement_size(snake);
			snake_decrement_score(snake);
			printf("Snake %d : %d", snake->id, snake->score);
		}
		
		// On retire le block de la map
		blocklist_remove_free(blockList, block);
	}
}

/*Fait les verifications sur la position d'un snake et des blocks*/
static void check_snake_position(Snake * snake, SnakeList * snakeList, BlockList * blockList) {
	blocklist_foreach(blockList, (Func_Blocklist_foreach_callback) check_snake_vs_block, snake);
}

/*Change de facon aleatoire la position d'un snake
Sauf s'il est un joueur*/
static void change_direction_random(Snake * snake, SnakeList * snakeList, void * data) {
	if(!snake->isComputer) {
		return;
	}
	int direction = rand()%4;
	switch(direction) {
		case 0:
			if(snake->direction == LEFT)
				return;
			snake_change_direction(snake, RIGHT);
			break;
		case 1:
			if(snake->direction == RIGHT)
				return;
			snake_change_direction(snake, LEFT);
			break;
		case 2:
			if(snake->direction == DOWN)
				return;
			snake_change_direction(snake, UP);
			break;
		case 3:
			if(snake->direction == UP)
				return;
			snake_change_direction(snake, DOWN);
			break;
	}
}

/**Marque le snake comme mort
 *  s'il sa taille est negative ou nulle.*/
static void remove_dead_snakes(
	Snake * snake, SnakeList * snakeList, void * data) {
	if(snake->size <= 0) {
		snake->isAlive = FALSE;
	}
}

static short areAllPlayerDead(Map * m) {
	for(unsigned i=0; i<m->playerCount && i<m->snakeList->total; i++) {
		if(m->snakeList->liste[i] != NULL && m->snakeList->liste[i]->isAlive)
			return FALSE;
	}
	return TRUE;
}

static short areAllBlockGone(Map * m) {
	return m->blockList->size == 0;
}

void play_round(Map * m) {
	if(m->isPaused) return;
	snakelist_foreach(m->snakeList, (Func_Snakelist_foreach_callback) change_direction_random, NULL);
	snakelist_foreach(m->snakeList, (Func_Snakelist_foreach_callback) move_snake, m);
	snakelist_foreach(m->snakeList, (Func_Snakelist_foreach_callback) check_snake_position, m->blockList);
	snakelist_foreach(m->snakeList, (Func_Snakelist_foreach_callback) remove_dead_snakes, NULL);

	if(areAllPlayerDead(m) || areAllBlockGone(m)) {
		m->isPaused = TRUE;
	}
}
