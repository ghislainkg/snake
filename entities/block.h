#ifndef _BLOCK_
#define _BLOCK_

#include "../help_structs/help_model.h"

/* Definition des types de block*/
typedef enum BlockType {
	HELL,
	LIFE
} BlockType;

/* Definition d'un block*/
typedef struct Block {
	Point * points;
	unsigned size;
	BlockType type;
} Block;

/* Cree un block d'un seul point*/
Block * block_create_single(int x, int y, BlockType type);
/* Cree un block formant un carre d'epaisseur thickness*/
Block * block_create_square(int x, int y, unsigned thickness, BlockType type);
/* Detruit un block*/
void block_destroy(Block * b);
/* Retourne la couleur associee a un type de block*/
Color block_get_color_for_type(BlockType type);
short block_collision_with(Block * block, Point point);

/*IMPLEMENTATION DE LISTE DE BLOCK*/

typedef struct BlockList {
	Block ** liste;
	unsigned int size;
	unsigned int total;
} BlockList;
#define BLOCK_LIST_SIZE_LIMIT 100

typedef void (*Func_Blocklist_foreach_callback)(Block *b, BlockList * blockList, void * data);
typedef void (*Func_Blocklist_foreach_index_callback)(int index, Block *b, BlockList * blockList, void * data);

BlockList * blocklist_create(int count);
void blocklist_add(BlockList *l, Block *b);
void blocklist_remove_free(BlockList *l, Block *b);
void blocklist_destroy(BlockList * b);
void blocklist_foreach(BlockList * l, Func_Blocklist_foreach_callback callback, void * data);
void blocklist_foreach_index(BlockList * l, Func_Blocklist_foreach_index_callback callback, void * data);

#endif