#include "drawing.h"

void draw_dot_on_context(
	cairo_t * context,
	int x, int y,
	Color color) {	

	x *= DOT_SIZE;
	y *= DOT_SIZE;
	cairo_set_source_rgba(context, color.r, color.g, color.b, 1.f);
	cairo_rectangle(
		context,
		x - DOT_SIZE_HALF,
		y - DOT_SIZE_HALF,
		DOT_SIZE,
		DOT_SIZE);

	cairo_fill(context);

	cairo_set_source_rgba(context, 0.f, 0.f, 0.f, 1.f);
}

void draw_line_on_context(
	cairo_t * context,
	int sx, int sy, int ex, int ey,
	Color color) {
	
	sx *= DOT_SIZE;
	sy *= DOT_SIZE;
	ex *= DOT_SIZE;
	ey *= DOT_SIZE;
	cairo_set_source_rgba(
		context,
		color.r, color.g, color.b, 1.f);
	cairo_rectangle(
		context,
		sx - DOT_SIZE_HALF,
		sy - DOT_SIZE_HALF,
		ex + DOT_SIZE_HALF,
		ey + DOT_SIZE_HALF);

	cairo_fill(context);	
}

void draw_snake(cairo_t * context, Snake * snake) {
	for (int i = 0; i < snake->size; ++i) {
		int x = snake->points[i].x;
		int y = snake->points[i].y;
		draw_dot_on_context(
			context,
			x, y, snake->color);
	}
}

void draw_block(cairo_t * context, Block * block) {
	for(unsigned i=0; i<block->size; i++) {
		draw_dot_on_context(
			context,
			block->points[i].x, block->points[i].y,
			block_get_color_for_type(block->type));
	}	
}

void draw_limits(
	cairo_t * context,
	int limitR, int limitL, int limitU, int limitD) {
/*	// UP LINE
	draw_line_on_context(context, limitL, limitU, limitR, limitU);
	// DOWN LINE
	draw_line_on_context(context, limitL, limitD, limitR, limitD);
	// RIGHT LINE
	draw_line_on_context(context, limitL, limitU, limitR, limitU);
	// LEFT LINE
	draw_line_on_context(context, );*/
}

static void drawMapBlocks(
	Block * b, BlockList * blockList, void * data) {
	cairo_t * context = (cairo_t *) data;
	draw_block(context, b);
}
static void drawMapSnakes(
	Snake * s, SnakeList * snakeList, void * data) {
	if(!s->isAlive) return;
	cairo_t * context = (cairo_t *) data;
	draw_snake(context, s);
}
void draw_map(
	cairo_t * context, Map * map) {
	draw_limits(
		context,
		map->limitR, map->limitL, map->limitU, map->limitD);
	blocklist_foreach(map->blockList, drawMapBlocks, context);
	snakelist_foreach(map->snakeList, drawMapSnakes, context);
}

