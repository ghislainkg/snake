#ifndef _DRAWING_
#define _DRAWING_

#include <gtk/gtk.h>

#include "../entities/map.h"

/*can be modify to change drawing resolution*/
#define DOT_SIZE 10.f
#define DOT_SIZE_HALF 5.f

void draw_dot_on_context(cairo_t * context, int x, int y, Color color);
void draw_line_on_context(cairo_t * context, int sx, int sy, int ex, int ey, Color color);

void draw_snake(cairo_t * context, Snake * snake);
void draw_block(cairo_t * context, Block * block);
void draw_limits(cairo_t * context, int limitR, int limitL, int limitU, int limitD);
void draw_map(cairo_t * context, Map * map);

#endif